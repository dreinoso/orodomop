package com.chainreaction.orodomop.ui.controlers;

import android.util.Log;

import java.util.HashSet;

public abstract class TimerControllerAbstract implements TimerController,
                                                               SecondListener,
                                                               TimerStateNotifier{

    private final String TAG = this.getClass().getName();
    protected final int START_SECONDS_TO_COUNT = 59;
    protected final int START_SECONDS_TO_SHOW = 00;
    protected int startMinutes;
    protected int minutes;
    protected int seconds;
    protected HashSet<TimerStateListener> timerlisteners;
    protected SecondsCounter counter;

    protected TimerControllerAbstract(){
        resetTimer();
        counter = new SecondsCounter();
        counter.register(this);
        timerlisteners = new HashSet<>();
    }

    @Override
    public void onSecondElapsed() {
        countSecond();
    }

    protected void countSecond() {
        --seconds;
        if (seconds < 0) {
            seconds = START_SECONDS_TO_COUNT;
            countMinute();
        }
        notifySecondElapsed();
    }

    protected void countMinute() {
        --minutes;
        if (minutes < 0) {
            resetTimer();
            notifyPomodoroTimerExpired();
        }
    }

    public void notifySecondElapsed() {
        for (TimerStateListener listener : timerlisteners) {
            listener.onSecondElapsed(minutes, seconds);
        }
    }

    public void notifyPomodoroTimerExpired() {
        for (TimerStateListener listener : timerlisteners) {
            listener.onTimerFinished(startMinutes, START_SECONDS_TO_SHOW);
        }
    }

    @Override
    public void startTimer() {
        counter.startCounting();
    }

    @Override
    public void stopTimer() {
        counter.stopCounting();
    }

    @Override
    public void resetTimer() {
        minutes = startMinutes;
        seconds = 0;
    }

    @Override
    public void setInitialTime(int newStartMinutes) {
        Log.d(TAG, "setInitialTime(): Minutes => " + newStartMinutes);
        startMinutes = newStartMinutes;
    }

    @Override
    public void setCurrentTime(int newMinutes, int newSeconds) {
        Log.d(TAG, "setCurrentTime(): " + newMinutes + ":" + newSeconds);
        minutes = newMinutes;
        seconds = newSeconds;
    }

    @Override
    public void registerTimerState(TimerStateListener listener) {
        timerlisteners.add(listener);
    }

    @Override
    public void unregisterTimerState(TimerStateListener listener) {
        timerlisteners.remove(listener);
    }

    @Override
    public int getMinutes() {
        return minutes;
    }

    @Override
    public int getSeconds() {
        return seconds;
    }
}
