package com.chainreaction.orodomop.ui.pomodorostatemachine;

public interface PomodoroStatesSupplier {

    PlayingState getPlayingState();

    PausedState getPausedState();

    StoppedState getStoppedState();

    BreakTimeState getBreakTimeState();
}
