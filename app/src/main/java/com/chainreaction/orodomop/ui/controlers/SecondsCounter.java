package com.chainreaction.orodomop.ui.controlers;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class SecondsCounter {

    private final String TAG = this.getClass().getName();
    private final long INTERVAL_PERIOD = 1000;
    private SecondListener listener;
    private Timer timer;
    private boolean timerIsRunning;
    private TimerTask timerTask;

    public SecondsCounter() {
        timer = new Timer();
    }

    public boolean register(SecondListener newListener) {
        boolean result = false;
        if (listener == null) {
            listener = newListener;
            result = true;
        }
        return result;
    }

    public void unregister() {
        stopCounting();
        listener = null;
    }

    public void startCounting() {
        Log.d(TAG, "startCounting");
        if (!timerIsRunning) {
            timerTask = new NotificationTask();
            timer.schedule(timerTask, 0, INTERVAL_PERIOD);
            timerIsRunning = true;
        }
    }

    public void stopCounting() {
        Log.d(TAG, "stopCounting");
        if (timerIsRunning) {
            timerTask.cancel();
            timerIsRunning = false;
        }
    }

    private class NotificationTask extends TimerTask {
        @Override
        public void run() {
            if (listener != null) {
                listener.onSecondElapsed();
            }
        }
    }
}
