package com.chainreaction.orodomop.ui.pomodorostatemachine;

public class PomodoroStateInstances implements PomodoroStatesSupplier {

    public PlayingState playingStateInstance;
    public PausedState pausedStateInstance;
    public StoppedState stoppedStateInstance;
    public BreakTimeState breakTimeState;

    private static PomodoroStateInstances instance;

    private PomodoroStateInstances() {
        playingStateInstance = new PlayingState(this);
        pausedStateInstance = new PausedState(this);
        stoppedStateInstance = new StoppedState(this);
        breakTimeState = new BreakTimeState(this);
    }

    public static PomodoroStateInstances getInstance() {
        if (instance == null) {
            synchronized (PomodoroStateInstances.class) {
                if (instance == null) {
                    instance = new PomodoroStateInstances();
                }
            }
        }

        return instance;
    }

    @Override
    public PlayingState getPlayingState() {
        return playingStateInstance;
    }

    @Override
    public PausedState getPausedState() {
        return pausedStateInstance;
    }

    @Override
    public StoppedState getStoppedState() {
        return stoppedStateInstance;
    }

    @Override
    public BreakTimeState getBreakTimeState() {
        return breakTimeState;
    }
}
