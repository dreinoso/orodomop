package com.chainreaction.orodomop.ui.controlers;

import com.chainreaction.orodomop.ui.pomodorostatemachine.PlayingState;
import com.chainreaction.orodomop.ui.pomodorostatemachine.PomodoroBaseState;
import com.chainreaction.orodomop.ui.pomodorostatemachine.PomodoroStateMachine;
import com.chainreaction.orodomop.ui.pomodorostatemachine.StoppedState;

public class PomodoroTimerController extends TimerControllerAbstract {

    private final String TAG = this.getClass().getName();
    private final int DEFAULT_START_MINUTES = 25;

    private static PomodoroTimerController instance;

    private PomodoroTimerController(){
        super();
        startMinutes = DEFAULT_START_MINUTES;
    }

    public static PomodoroTimerController getInstance() {
        if (instance == null) {
            synchronized (PomodoroTimerController.class) {
                if (instance == null) {
                    instance = new PomodoroTimerController();
                }
            }
        }

        return instance;
    }

    @Override
    public int getDefaultStartMinutes() {
        return DEFAULT_START_MINUTES;
    }

    @Override
    public boolean isPlaying() {
        PomodoroBaseState currentState = PomodoroStateMachine.getInstance().getCurrentState();
        boolean isPlaying = currentState instanceof PlayingState;
        return isPlaying;
    }

    @Override
    public boolean isStopped() {
        PomodoroBaseState currentState = PomodoroStateMachine.getInstance().getCurrentState();
        boolean isStopped = currentState instanceof StoppedState;
        return isStopped;
    }
}
