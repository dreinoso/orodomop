package com.chainreaction.orodomop.ui.pomodorostatemachine;

import com.chainreaction.orodomop.ui.controlers.BreakTimerController;
import com.chainreaction.orodomop.ui.controlers.TimerController;

public class BreakTimeState extends PomodoroBaseState {

    TimerController breakTimerController = BreakTimerController.getInstance();

    public BreakTimeState(PomodoroStatesSupplier pomodoroStatesSuppiler) {
        super(pomodoroStatesSuppiler);
    }

    @Override
    public StoppedState stopBreakTime() {
        breakTimerController.stopTimer();
        pomodoroController.resetTimer();
        return statesSuppiler.getStoppedState();
    }
}
