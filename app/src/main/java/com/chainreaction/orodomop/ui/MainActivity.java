package com.chainreaction.orodomop.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.chainreaction.orodomop.R;
import com.chainreaction.orodomop.ui.controlers.BreakTimerController;
import com.chainreaction.orodomop.ui.controlers.TimerStateNotifier;
import com.chainreaction.orodomop.ui.controlers.TimerStateListener;
import com.chainreaction.orodomop.ui.controlers.PomodoroTimerController;
import com.chainreaction.orodomop.ui.controlers.TimerController;
import com.chainreaction.orodomop.ui.pomodorostatemachine.PomodoroBaseState;
import com.chainreaction.orodomop.ui.pomodorostatemachine.PomodoroStateMachine;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import helpers.PreferenceKeysHolder;

public class MainActivity extends AppCompatActivity {

    final String TAG = this.getClass().getName();

    PomodoroStateMachine pomodoroStateMachine = PomodoroStateMachine.getInstance();

    TimerStateNotifier pomodoroStateNotifier = PomodoroTimerController.getInstance();
    TimerStateNotifier breakTimerStateNotifier = BreakTimerController.getInstance();

    PomodoroTimerListener pomodoroTimerListener = new PomodoroTimerListener();
    BreakTimerListener breakTimerListener = new BreakTimerListener();

    TimerController pomodoroTimerController = PomodoroTimerController.getInstance();
    TimerController breakTimerController = BreakTimerController.getInstance();

    ImageButton playPauseButton;
    ImageButton resumeButton;
    ImageButton stopButton;
    Button skipBreakButton;
    TextView pomodoroClock;
    ProgressBar pomodoroProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linkUiViews();
        setClickListeners();
        setInitialVisibilities();
        setTimesFromPreferences();
        ressetTimerToShowInitialTime();
    }

    private void linkUiViews() {
        Toolbar toolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        playPauseButton = findViewById(R.id.central_reproduction_button);
        resumeButton = findViewById(R.id.left_reproduction_button);
        stopButton = findViewById(R.id.right_reproduction_button);
        skipBreakButton = findViewById(R.id.skip_break_button);
        pomodoroClock = findViewById(R.id.pomodoro_clock);
        pomodoroProgressBar = findViewById(R.id.pomodoro_progress_bar);
    }

    private void setClickListeners() {
        playPauseButton.setOnClickListener(new PlayPauseButtonListener());
        resumeButton.setOnClickListener(new ResumeButtonListener());
        stopButton.setOnClickListener(new StopButtonListener());
        skipBreakButton.setOnClickListener(new SkipBreakButtonListener());
    }

    private void setInitialVisibilities() {
        pomodoroProgressBar.setVisibility(View.INVISIBLE);
    }

    private void ressetTimerToShowInitialTime() {
        if(pomodoroStateNotifier.isStopped()) {
            pomodoroTimerController.resetTimer();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerListeners();
        updateTimeToShow();
        setVisibilityOfButtons();
    }

    private void registerListeners() {
        pomodoroStateNotifier.registerTimerState(pomodoroTimerListener);
        if (isBreakTimeEnabled()) {
            breakTimerStateNotifier.registerTimerState(breakTimerListener);
        }
    }

    private boolean isBreakTimeEnabled() {
        // todo get value from preference
        return true;
    }

    private void updateTimeToShow() {
        if(!breakTimerController.isPlaying()) {
            setCurrentPomodoroTime();
        } else {
            setCurrentBreakTime();
        }
    }

    private void setCurrentPomodoroTime() {
        Log.d(TAG, "setCurrentPomodoroTime");
        setTimeToClockView(pomodoroStateNotifier.getMinutes(),
                pomodoroStateNotifier.getSeconds());
    }

    private void setCurrentBreakTime() {
        Log.d(TAG, "setCurrentBreakTime");
        setTimeToClockView(breakTimerStateNotifier.getMinutes(),
                breakTimerStateNotifier.getSeconds());
    }

    private void setTimeToClockView(int minutes, int seconds) {
        final String timeToShow = formatTimeToShow(minutes, seconds);
        Log.d(TAG, "setTimeToClockView(): " + timeToShow);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pomodoroClock.setText(timeToShow);
            }
        });
    }

    private String formatTimeToShow(int minutes, int seconds) {
        String minutesFormtted = String.valueOf(minutes);
        String secondsFormatted = String.valueOf(seconds);
        if (minutes < 10) {
            minutesFormtted = "0" + minutesFormtted;
        }

        if (seconds < 10) {
            secondsFormatted = "0" + secondsFormatted;
        }

        String timeToShow = minutesFormtted + ":" + secondsFormatted;
        return timeToShow;
    }

    private void setVisibilityOfButtons() {
        if (pomodoroStateNotifier.isStopped()) {
            setViewsForStoppedState();
        } else if (pomodoroStateNotifier.isPlaying()) {
            setViewsForPlayingState();
        } else if (breakTimerStateNotifier.isPlaying()) {
            setViewsForBreakTimeState();
        } else {
            setViewsForPausedState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterListeners();
    }

    private void unregisterListeners() {
        pomodoroStateNotifier.unregisterTimerState(pomodoroTimerListener);
        if (isBreakTimeEnabled()) {
            breakTimerStateNotifier.unregisterTimerState(breakTimerListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent settingIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingIntent);
                return true;
            case R.id.action_about:
                Intent aboutIntent  = new Intent(this, AboutActivity.class);
                startActivity(aboutIntent);
                return true;
            case R.id.action_debug_reduce_time: // todo remove for release
                pomodoroTimerController.setCurrentTime(0,10);
                breakTimerController.setCurrentTime(0,10);
                return true;
            case R.id.action_exit:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        goHomeInsteadOfPreviousActivity();
    }

    private void goHomeInsteadOfPreviousActivity() {
        Intent goHomeIntent = new Intent(Intent.ACTION_MAIN);
        goHomeIntent.addCategory(Intent.CATEGORY_HOME);
        goHomeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(goHomeIntent);
    }

    private class PomodoroTimerListener implements TimerStateListener {

        @Override
        public void onTimerFinished(int initialMinutes, int initialSeconds) {
            // todo check if break time is enabled
            pomodoroStateMachine.playBreakTime();
            updateTimeToShow();
            setViewsForBreakTimeState();
        }

        @Override
        public void onSecondElapsed(int currentMinutes, int currentSeconds) {
            updateTimeToShow();
        }
    }

    private class BreakTimerListener implements TimerStateListener {

        @Override
        public void onTimerFinished(int initialMinutes, int initialSeconds) {
            pomodoroStateMachine.stopBreakTime();
            updateTimeToShow();
            setViewsForStoppedState();
        }

        @Override
        public void onSecondElapsed(int currentMinutes, int currentSeconds) {
            updateTimeToShow();
        }
    }

    private class PlayPauseButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (pomodoroStateNotifier.isStopped()) {
                pomodoroStateMachine.play();
                setViewsForPlayingState();
            } else {
                pomodoroStateMachine.pause();
                setViewsForPausedState();
            }
        }
    }

    private class ResumeButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            pomodoroStateMachine.resume();
            setViewsForPlayingState();
        }
    }

    private class StopButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            pomodoroStateMachine.stop();
            setViewsForStoppedState();
            setPomodoroTimeFromPreferences();
            updateTimeToShow();
        }
    }

    private void setTimesFromPreferences() {
        setPomodoroTimeFromPreferences();
        setBreakTimeFromPreferences();
    }

    private void setPomodoroTimeFromPreferences() {
        String defaultMinutes = String.valueOf(pomodoroStateNotifier.getDefaultStartMinutes());
        int minutesFromPreference = getMinutesFromPreference(
                PreferenceKeysHolder.POMODORO_TIME_LIST,
                defaultMinutes);
        pomodoroTimerController.setInitialTime(minutesFromPreference);
    }

    private int getMinutesFromPreference(String key, String defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String pomodoroTime = (sharedPreferences.getString(key, defaultValue));
        int minutes = Integer.valueOf(pomodoroTime);
        return minutes;
    }

    private class SkipBreakButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            pomodoroStateMachine.stopBreakTime();
            setViewsForStoppedState();
            setPomodoroTimeFromPreferences();
            updateTimeToShow();
        }
    }

    private void setBreakTimeFromPreferences() {
        String defaultMinutes = String.valueOf(breakTimerStateNotifier.getDefaultStartMinutes());
        int minutesFromPreference = getMinutesFromPreference(PreferenceKeysHolder.BREAK_TIME_LIST,
                                                             defaultMinutes);
        breakTimerController.setInitialTime(minutesFromPreference);
    }

    private void setViewsForStoppedState() {
        showOnlyCentralOfButtons();
        // todo load play image
    }

    private void setViewsForPlayingState() {
        showOnlyCentralOfButtons();
        // todo load pause image
    }

    private void setViewsForBreakTimeState() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                playPauseButton.setVisibility(View.INVISIBLE);
                resumeButton.setVisibility(View.INVISIBLE);
                stopButton.setVisibility(View.INVISIBLE);
                skipBreakButton.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setViewsForPausedState() {
        showOnlyLeftAndRightOfButtons();
    }

    private void showOnlyCentralOfButtons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                playPauseButton.setVisibility(View.VISIBLE);
                resumeButton.setVisibility(View.INVISIBLE);
                stopButton.setVisibility(View.INVISIBLE);
                skipBreakButton.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void showOnlyLeftAndRightOfButtons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                playPauseButton.setVisibility(View.INVISIBLE);
                resumeButton.setVisibility(View.VISIBLE);
                stopButton.setVisibility(View.VISIBLE);
                skipBreakButton.setVisibility(View.INVISIBLE);
            }
        });
    }
}
