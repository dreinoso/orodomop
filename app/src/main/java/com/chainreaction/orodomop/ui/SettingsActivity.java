package com.chainreaction.orodomop.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;

import java.util.List;

import com.chainreaction.orodomop.R;
import com.chainreaction.orodomop.ui.controlers.BreakTimerController;
import com.chainreaction.orodomop.ui.controlers.PomodoroTimerController;
import com.chainreaction.orodomop.ui.controlers.TimerController;

import helpers.PreferenceKeysHolder;

/**
 * Presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {

    private final String TAG = this.getClass().getName();

    private static final TimerController pomodoroTimerController = PomodoroTimerController.getInstance();
    private static final TimerController breakTimerController = BreakTimerController.getInstance();

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener =
            new Preference.OnPreferenceChangeListener() {

        Preference currentPreference;
        String currentValue;

        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            currentPreference = preference;
            currentValue = value.toString();

            if (preference instanceof ListPreference) {
                lookUpCorrectPrefInListAndSet();
            } else if (preference instanceof RingtonePreference) {
                lookUpCorrectRingstonePrefAndSet();
            } else { // Other preferences have simple string representation
                preference.setSummary(currentValue);
            }
            return true;
        }

        private void lookUpCorrectPrefInListAndSet() {
            ListPreference listPreference = (ListPreference) currentPreference;
            int index = listPreference.findIndexOfValue(currentValue);
            currentPreference.setSummary(
                    index >= 0
                            ? listPreference.getEntries()[index]
                            : null);
        }

        private void lookUpCorrectRingstonePrefAndSet() {
            if (TextUtils.isEmpty(currentValue)) { // Empty values correspond to 'silent'
                currentPreference.setSummary(R.string.pref_ringtone_silent);
            } else {
                Ringtone ringtone = RingtoneManager.getRingtone(
                        currentPreference.getContext(), Uri.parse(currentValue));
                if (ringtone == null) { // There was a lookup error, so clean the summary
                    currentPreference.setSummary(null);
                } else { // Set new ringtone
                    String name = ringtone.getTitle(currentPreference.getContext());
                    currentPreference.setSummary(name);
                }
            }
        }
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
        String newPreferenceValue = PreferenceManager.
                                    getDefaultSharedPreferences(preference.getContext()).
                                    getString(preference.getKey(), "");
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,newPreferenceValue);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result;
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            result = true;
        } else {
            result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || TimesPreferenceFragment.class.getName().equals(fragmentName)
                || ThemePreferenceFragment.class.getName().equals(fragmentName)
                || NotificationPreferenceFragment.class.getName().equals(fragmentName);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class TimesPreferenceFragment extends PreferenceFragment implements
            SharedPreferences.OnSharedPreferenceChangeListener {

        final String TAG = this.getClass().getName();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_times);
            setHasOptionsMenu(true);
            bindPreferenceSummaryToValue(findPreference(PreferenceKeysHolder.POMODORO_TIME_LIST));
            bindPreferenceSummaryToValue(findPreference(PreferenceKeysHolder.BREAK_TIME_LIST));
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Log.d(TAG, "onSharedPreferenceChanged: key is => " + key);
            switch (key) {
                case PreferenceKeysHolder.POMODORO_TIME_LIST:
                    setPomodoroTimeOnPomodoroTimer(sharedPreferences);
                    break;
                case PreferenceKeysHolder.BREAK_TIME_LIST:
                    setBreakTimeOnPomodoroTimer(sharedPreferences);
                    break;
                default:
                    throw new IllegalStateException("No key matched for TimesPreferenceFragment");
            }
        }

        private void setPomodoroTimeOnPomodoroTimer(SharedPreferences preference) {
            String pomodoroTime = preference.getString(PreferenceKeysHolder.POMODORO_TIME_LIST, "25");
            int minutes = Integer.valueOf(pomodoroTime);
            pomodoroTimerController.setInitialTime(minutes);
        }

        private void setBreakTimeOnPomodoroTimer(SharedPreferences preference) {
            String breakTime = preference.getString(PreferenceKeysHolder.BREAK_TIME_LIST, "25");
            int minutes = Integer.valueOf(breakTime);
            breakTimerController.setInitialTime(minutes);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class NotificationPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_notification);
            setHasOptionsMenu(true);
            bindPreferenceSummaryToValue(findPreference(
                    PreferenceKeysHolder.RINGTONE_PREFERENCE_SELECTOR));
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class ThemePreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_theme);
            setHasOptionsMenu(true);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
}
