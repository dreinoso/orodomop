package com.chainreaction.orodomop.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.chainreaction.orodomop.R;

public class LicenseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license);

        TextView content = (TextView) findViewById(R.id.textView_content);
        content.setText(R.string.license);
    }
}
