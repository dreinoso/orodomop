package com.chainreaction.orodomop.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.chainreaction.orodomop.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setupActionBar();
        Button buttonLicense = (Button) findViewById(R.id.button_license);
        buttonLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent licenseIntent = new Intent(v.getContext(), LicenseActivity.class);
                startActivity(licenseIntent);
            }
        });

        ImageView chainReactionImage = (ImageView) findViewById(R.id.imageChainReaction);
        chainReactionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo change to real web page
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(browserIntent);
            }
        });
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result;
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            result = true;
        } else {
            result = super.onOptionsItemSelected(item);
        }
        return result;
    }
}
