package com.chainreaction.orodomop.ui.pomodorostatemachine;

public interface PomodoroStateTransitions {
    PomodoroBaseState play();

    PomodoroBaseState pause();

    PomodoroBaseState stop();

    PomodoroBaseState resume();

    PomodoroBaseState playBreakTime();

    PomodoroBaseState stopBreakTime();
}
