package com.chainreaction.orodomop.ui.pomodorostatemachine;

public class StoppedState extends PomodoroBaseState {

    public StoppedState(PomodoroStatesSupplier pomodoroStatesSuppiler) {
        super(pomodoroStatesSuppiler);
    }

    @Override
    public PlayingState play() {
        pomodoroController.resetTimer();
        pomodoroController.startTimer();
        return statesSuppiler.getPlayingState();
    }
}
