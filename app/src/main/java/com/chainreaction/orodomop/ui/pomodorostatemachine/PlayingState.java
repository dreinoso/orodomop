package com.chainreaction.orodomop.ui.pomodorostatemachine;

import com.chainreaction.orodomop.ui.controlers.BreakTimerController;
import com.chainreaction.orodomop.ui.controlers.TimerController;

public class PlayingState extends PomodoroBaseState {

    TimerController breakTimerController = BreakTimerController.getInstance();

    public PlayingState(PomodoroStatesSupplier pomodoroStatesSupplier) {
        super(pomodoroStatesSupplier);
    }

    @Override
    public PausedState pause() {
        pomodoroController.stopTimer();
        return statesSuppiler.getPausedState();
    }


    @Override
    public BreakTimeState playBreakTime() {
        pomodoroController.stopTimer();
        breakTimerController.resetTimer();
        breakTimerController.startTimer();
        return statesSuppiler.getBreakTimeState();
    }
}
