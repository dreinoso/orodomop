package com.chainreaction.orodomop.ui.pomodorostatemachine;

public class PausedState extends PomodoroBaseState {

    public PausedState(PomodoroStatesSupplier pomodoroStatesSuppiler) {
        super(pomodoroStatesSuppiler);
    }

    @Override
    public StoppedState stop() {
        pomodoroController.stopTimer();
        pomodoroController.resetTimer();
        return statesSuppiler.getStoppedState();
    }

    @Override
    public PlayingState resume() {
        pomodoroController.startTimer();
        return statesSuppiler.getPlayingState();
    }
}
