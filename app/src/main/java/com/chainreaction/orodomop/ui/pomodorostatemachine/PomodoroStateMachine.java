package com.chainreaction.orodomop.ui.pomodorostatemachine;

import android.util.Log;

public class PomodoroStateMachine implements PomodoroStateTransitions {

    private final String TAG = this.getClass().getName();
    private static PomodoroStateMachine instance;
    PomodoroStatesSupplier statesSuppiler;
    PomodoroBaseState currentState;

    private PomodoroStateMachine() {
        statesSuppiler = PomodoroStateInstances.getInstance();
        currentState = statesSuppiler.getStoppedState(); // Stopped is the initial state
    }

    public static PomodoroStateMachine getInstance() {
        if (instance == null) {
            synchronized (PomodoroStateMachine.class) {
                if (instance == null) {
                    instance = new PomodoroStateMachine();
                }
            }
        }

        return instance;
    }

    public PomodoroBaseState getCurrentState() {
        return currentState;
    }

    @Override
    public PomodoroBaseState play() {
        Log.d(TAG, "play()");
        currentState = currentState.play();
        return currentState;
    }

    @Override
    public PomodoroBaseState pause() {
        Log.d(TAG, "pause()");
        currentState = currentState.pause();
        return currentState;
    }

    @Override
    public PomodoroBaseState stop() {
        Log.d(TAG, "stop()");
        currentState = currentState.stop();
        return currentState;
    }

    @Override
    public PomodoroBaseState resume() {
        Log.d(TAG, "resume()");
        currentState = currentState.resume();
        return currentState;
    }

    @Override
    public PomodoroBaseState playBreakTime() {
        Log.d(TAG, "playBreakTime()");
        currentState = currentState.playBreakTime();
        return currentState;
    }

    @Override
    public PomodoroBaseState stopBreakTime() {
        Log.d(TAG, "stopBreakTime()");
        currentState = currentState.stopBreakTime();
        return currentState;
    }
}
