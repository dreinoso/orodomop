package com.chainreaction.orodomop.ui.controlers;

public interface TimerStateListener {

    void onTimerFinished(int initialMinutes, int initialSeconds);

    void onSecondElapsed(int currentMinutes, int currentSeconds);
}
