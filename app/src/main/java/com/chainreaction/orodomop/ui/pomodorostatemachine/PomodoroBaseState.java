package com.chainreaction.orodomop.ui.pomodorostatemachine;

import com.chainreaction.orodomop.ui.controlers.PomodoroTimerController;
import com.chainreaction.orodomop.ui.controlers.TimerController;

public class PomodoroBaseState implements PomodoroStateTransitions {

    protected PomodoroStatesSupplier statesSuppiler;
    protected TimerController pomodoroController = PomodoroTimerController.getInstance();

    private String exeptionMessage = "Transition not supported from the current state";

    public PomodoroBaseState(PomodoroStatesSupplier pomodoroStatesSuppiler) {
        statesSuppiler = pomodoroStatesSuppiler;
    }

    @Override
    public PomodoroBaseState play() {
        throw new UnsupportedOperationException(exeptionMessage);
    }

    @Override
    public PomodoroBaseState pause() {
        throw new UnsupportedOperationException(exeptionMessage);
    }

    @Override
    public PomodoroBaseState stop() {
        throw new UnsupportedOperationException(exeptionMessage);
    }

    @Override
    public PomodoroBaseState resume() {
        throw new UnsupportedOperationException(exeptionMessage);
    }

    @Override
    public PomodoroBaseState stopBreakTime() {
        throw new UnsupportedOperationException(exeptionMessage);
    }

    @Override
    public PomodoroBaseState playBreakTime() {
        throw new UnsupportedOperationException(exeptionMessage);
    }
}
