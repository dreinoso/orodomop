package com.chainreaction.orodomop.ui.controlers;

public interface TimerStateNotifier {

    void registerTimerState(TimerStateListener listener);

    void unregisterTimerState(TimerStateListener listener);

    int getMinutes();

    int getDefaultStartMinutes();

    int getSeconds();

    boolean isPlaying();

    boolean isStopped();
}
