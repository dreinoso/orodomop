package com.chainreaction.orodomop.ui.controlers;

import com.chainreaction.orodomop.ui.pomodorostatemachine.BreakTimeState;
import com.chainreaction.orodomop.ui.pomodorostatemachine.PomodoroBaseState;
import com.chainreaction.orodomop.ui.pomodorostatemachine.PomodoroStateMachine;

public class BreakTimerController extends TimerControllerAbstract {

    private final String TAG = this.getClass().getName();
    private final int DEFAULT_START_MINUTES = 5;

    private static BreakTimerController instance;

    private BreakTimerController(){
        super();
        startMinutes = DEFAULT_START_MINUTES;
    }

    public static BreakTimerController getInstance() {
        if (instance == null) {
            synchronized (BreakTimerController.class) {
                if (instance == null) {
                    instance = new BreakTimerController();
                }
            }
        }

        return instance;
    }

    @Override
    public int getDefaultStartMinutes() {
        return DEFAULT_START_MINUTES;
    }

    @Override
    public boolean isPlaying() {
        PomodoroBaseState currentState = PomodoroStateMachine.getInstance().getCurrentState();
        boolean isPlaying = currentState instanceof BreakTimeState;
        return isPlaying;
    }

    @Override
    public boolean isStopped() {
        PomodoroBaseState currentState = PomodoroStateMachine.getInstance().getCurrentState();
        boolean isStopped = !(currentState instanceof BreakTimeState);
        return isStopped;
    }
}
