package com.chainreaction.orodomop.ui.controlers;

public interface TimerController {

    void startTimer();

    void stopTimer();

    void resetTimer();

    boolean isPlaying();

    void setInitialTime(int startMinutes);

    // Should only be used for debugging
    void setCurrentTime(int minutes, int seconds);
}
