package helpers;

public final class PreferenceKeysHolder {
    public static final String POMODORO_TIME_LIST = "pomdoro_time_list";
    public static final String BREAK_TIME_LIST = "break_time_list";
    public static final String RINGTONE_PREFERENCE_SELECTOR = "ringtone_preference_selector";
}
